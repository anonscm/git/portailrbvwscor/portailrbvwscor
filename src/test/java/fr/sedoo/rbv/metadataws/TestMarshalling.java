package fr.sedoo.rbv.metadataws;

import javax.xml.bind.Marshaller;

import org.geotoolkit.xml.MarshallerPool;
import org.junit.Test;

public class TestMarshalling {

	
	@Test
	public void testToRBV() throws Exception
	{
		MarshallerPool pool = new MarshallerPool(RBVMetadata.class);
		Marshaller marshaller = pool.acquireMarshaller();
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		marshaller.marshal(metadata, System.out);
	}
	
	
}
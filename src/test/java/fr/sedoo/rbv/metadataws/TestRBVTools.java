package fr.sedoo.rbv.metadataws;

import junit.framework.Assert;

import org.junit.Test;

import fr.sedoo.commons.metadata.utils.domain.MetadataTools;

public class TestRBVTools {

	
	@Test
	public void testToRBV() throws Exception
	{
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		System.out.println(RBVTools.toRBV(metadata));
	}
	
	@Test
	public void testfromRBV() throws Exception
	{
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		String str1 = RBVTools.toRBV(metadata);
		
		
		
		RBVMetadata metadata2 = (RBVMetadata) RBVTools.fromRBV(str1);
		String str2 = RBVTools.toRBV(metadata2);
		System.out.println("--------------Chaine 1 --------------");
		System.out.println(str1);
		System.out.println("--------------Chaine 2 --------------");
		System.out.println(str2);
		Assert.assertEquals("Les deux chaines doivent correspondre", str1, str2);
		String str3 = MetadataTools.toISO19139(metadata2);
		System.out.println("--------------Chaine 3 --------------");
		System.out.println(str3);
	}
}
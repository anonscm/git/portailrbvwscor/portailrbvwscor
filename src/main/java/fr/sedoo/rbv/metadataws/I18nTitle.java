package fr.sedoo.rbv.metadataws;

import java.util.ArrayList;

public class I18nTitle {
	
	private ArrayList<Translation> translations;

	public ArrayList<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(ArrayList<Translation> translations) {
		this.translations = translations;
	}


}

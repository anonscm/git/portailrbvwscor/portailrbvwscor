package fr.sedoo.rbv.metadataws;

import org.geotoolkit.metadata.iso.distribution.DefaultFormat;
import org.geotoolkit.util.DefaultInternationalString;


public class RBVFormat extends DefaultFormat {

	public void setStringName(String name)
	{
		super.setName(new DefaultInternationalString(name));
	}
	
	public void setStringVersion(String version)
	{
		super.setVersion(new DefaultInternationalString(version));
	}
	
	public String getStringName()
	{
		return super.getName().toString();
	}
	
	public String getStringVersion()
	{
		return super.getVersion().toString();
	}
	
}

package fr.sedoo.rbv.metadataws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.geotoolkit.gml.xml.TimeIndeterminateValueType;
import org.geotoolkit.metadata.iso.distribution.DefaultFormat;
import org.geotoolkit.metadata.iso.extent.DefaultGeographicBoundingBox;
import org.geotoolkit.util.DefaultInternationalString;
import org.opengis.metadata.citation.Role;
import org.opengis.metadata.identification.CharacterSet;
import org.opengis.metadata.identification.Progress;
import org.opengis.metadata.maintenance.MaintenanceFrequency;

import fr.sedoo.commons.metadata.shared.DefaultResourceIdentifier;
import fr.sedoo.commons.metadata.shared.DefaultResourceLink;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.commons.metadata.utils.domain.Contact;
import fr.sedoo.commons.metadata.utils.domain.DescribedURL;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;

public class MetadataSampleProvider 
{
	public static RBVMetadata getTestMetadata() throws Exception
	{
		RBVMetadata metadata = new RBVMetadata();
		
		
		/* Partie Identification
		 * 
		 * resourceTitle
		 * resourceAlternateTitle
		 * resourceAbstract
		 * resourceURL
		 * snapshotURL
		 * resourceContacts
		 * status
		 * updateRythm
		 */
		
		Level level = new Level();
		level.setName("MONOBS");
		level.setType(SedooMetadata.OBSERVATORY);
		
		metadata.setLevel(level);
		
		Map<Locale, String> titles = new HashMap<Locale, String>();
		titles.put(Locale.ENGLISH,"my title");
		titles.put(Locale.FRENCH,"mon titre");
		metadata.setResourceTitle(titles,"my title");
		
		Map<Locale, String> abstracts = new HashMap<Locale, String>();
		abstracts.put(Locale.FRENCH,"mon résumé");
		
		metadata.setResourceAbstract(abstracts, "mon résumé");
		
		final DefaultResourceLink link1 = new DefaultResourceLink();
		link1.setLabel("Google");
		link1.setLink("http://www.google.com");
		
		final DefaultResourceLink link2 = new RBVResourceLink();
		link2.setLabel("Le monde");
		link2.setLink("http://www.lemonde.fr");
		link2.setProtocol("information");
		
		final DefaultResourceLink link3 = new RBVResourceLink();
		link3.setLabel("Lien WFS fictif");
		link3.setLink("http://example.com/wfs?service=WFSSIMPLE&version=1.0.0&REQUEST=GetCapabilities");
		link3.setProtocol("wfs");
		
		metadata.setResourceURL(new ArrayList<DefaultResourceLink>() {{ add(link1); add(link2); add(link3); }});
		
		List<DescribedURL> snapshotURL= new ArrayList<DescribedURL>() {{ add(new DescribedURL("http://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Black_cherry_tree_histogram_no_title.svg/216px-Black_cherry_tree_histogram_no_title.svg.png", "snapshot1")); add(new DescribedURL("http://upload.wikimedia.org/wikipedia/commons/2/2e/Pie_chart_EP_election_2004.svg", "snapshot2")); }};
		metadata.setSnapshotURL(snapshotURL);
		
		Logo logo = new Logo();
		logo.setUrl("http://urldemonlogo");
		
		metadata.setLogo(logo);
		
		List<String> resourceLanguages= new ArrayList<String>() {{ add("eng"); add("fre");}};
		metadata.setResourceLanguages(resourceLanguages);
		
		final Contact contact1 = new Contact();
		contact1.setIndividualName("Jean Breille");
		contact1.setRole(Role.AUTHOR.name());
		contact1.setOrganisationName("IRD");
		contact1.setEmailAddress("jean.breille@ird.fr");
		
		final Contact contact2 = new Contact();
		contact2.setIndividualName("Jacques Célere");
		contact2.setRole(Role.OWNER.name());
		contact2.setOrganisationName("INRA");
		contact2.setEmailAddress("jacques.celere@inra.fr");
		
		List<Contact> contacts = new ArrayList<Contact>() {{ add(contact1); add(contact2); }};
		metadata.setResourceContacts(contacts);
		
		metadata.setStatus(Progress.PLANNED.name());
		metadata.setResourceUpdateRythm(MaintenanceFrequency.DAILY.name());
		
		DefaultResourceIdentifier id1 = new DefaultResourceIdentifier();
		id1.setNameSpace("http://isbn.fr");
		id1.setCode("123456789");
		
		DefaultResourceIdentifier id2 = new DefaultResourceIdentifier();
		id2.setNameSpace("http://sedoo.fr");
		id2.setCode("sedoo-wssdf");
		
		ArrayList<ResourceIdentifier> identifiers = new ArrayList<ResourceIdentifier>();
		identifiers.add(id1);
		identifiers.add(id2);
		
		metadata.addResourceIdentifiers(identifiers);
		
		/* Partie Identification
		 *
		 * metadataContacts
		 * metadataDate
		 * metadataLanguage
		 * uuid
		 * 
		*/
		
		final Contact metacontact1 = new Contact();
		metacontact1.setIndividualName("François ANDRE");
		metacontact1.setRole(Role.POINT_OF_CONTACT.name());
		metacontact1.setOrganisationName("OMP");
		metacontact1.setEmailAddress("francois.andre.prof@gmail.com");
		
		List<Contact> metacontacts = Collections.singletonList(metacontact1);
		metadata.setMetadataContacts(metacontacts);
		metadata.setMetadataDate("1995-12-01");
		metadata.setUuid("uuid-12345");
		metadata.setParentIdentifier("parentuuid-987654");
		metadata.setMetadataLanguage("fre");
		
		
		/* Partie spatiale 
		 * 
		 * 
		 */
		DefaultGeographicBoundingBox box = new DefaultGeographicBoundingBox();
		box.setNorthBoundLatitude(32L);
		box.setEastBoundLongitude(8L);
		box.setSouthBoundLatitude(4L);
		box.setWestBoundLongitude(-25L);
		
		metadata.addGeographicBoundingBox(box);
		
		DefaultGeographicBoundingBox box2 = new DefaultGeographicBoundingBox();
		box2.setNorthBoundLatitude(38L);
		box2.setEastBoundLongitude(16L);
		box2.setSouthBoundLatitude(8L);
		box2.setWestBoundLongitude(-20L);
		
		metadata.addGeographicBoundingBox(box2);
		
		
		/* Partie temporelle 
		 * 
		 * 
		 */
		
		metadata.setResourceBeginDate("2000-12-01");
		metadata.setResourceEndDate(TimeIndeterminateValueType.NOW.name());
		//metadata.setResourceEndDate("2010-12-01");
		
		/* Partie contraintes
		 * 
		 */
		
		Map<Locale, String> conditions = new HashMap<Locale, String>();
		conditions.put(Locale.ENGLISH,"my conditions");
		conditions.put(Locale.FRENCH,"mes conditions");
		
		Map<Locale, String> limitations = new HashMap<Locale, String>();
		limitations.put(Locale.ENGLISH,"my limitations");
		limitations.put(Locale.FRENCH,"mes limitations");
		
		metadata.setUseConditions(conditions, "mes conditions");
		metadata.setPublicAccessLimitations(limitations, "mes limitations");
		
		/*
		 * Partie autres 
		 */
		metadata.setResourceEncodingCharset(CharacterSet.UTF_8.identifier());
		DefaultFormat word97Format = new DefaultFormat();
		word97Format.setName(new DefaultInternationalString("word"));
		word97Format.setVersion(new DefaultInternationalString("97"));
		metadata.setResourceFormat(word97Format);
		metadata.setCreationDate("1975-01-24");
		metadata.setLastRevisionDate("1980-01-24");
		metadata.setPublicationDate("1985-01-24");
		metadata.setCoordinateSystem("EPSG:4326");
		
		return metadata;
	}
}

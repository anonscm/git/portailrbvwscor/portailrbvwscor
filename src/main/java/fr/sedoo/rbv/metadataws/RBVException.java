package fr.sedoo.rbv.metadataws;

public class RBVException extends Exception{

	public RBVException(Throwable t) 
	{
		super(t);
	}

}

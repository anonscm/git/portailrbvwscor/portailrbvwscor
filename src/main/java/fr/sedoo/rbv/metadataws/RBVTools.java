package fr.sedoo.rbv.metadataws;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;

public class RBVTools implements RBVConstants {
	
	public static String toRBV(RBVMetadata metadata) throws Exception
	{
		return RBVMarshaller.toRBV(metadata);
	}
	

	public static SedooMetadata fromRBV(String xml) throws Exception
	{
		return RBVUnmarshaller.fromRBV(xml);
	}

}

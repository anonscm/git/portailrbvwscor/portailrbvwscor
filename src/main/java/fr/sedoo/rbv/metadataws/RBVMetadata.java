package fr.sedoo.rbv.metadataws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.geotoolkit.metadata.iso.citation.DefaultCitation;
import org.geotoolkit.metadata.iso.constraint.DefaultConstraints;
import org.geotoolkit.metadata.iso.constraint.DefaultLegalConstraints;
import org.geotoolkit.metadata.iso.identification.DefaultDataIdentification;
import org.geotoolkit.util.SimpleInternationalString;
import org.opengis.metadata.maintenance.ScopeCode;

import fr.sedoo.commons.metadata.shared.DefaultResourceIdentifier;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.commons.metadata.utils.domain.DescribedURL;
import fr.sedoo.commons.metadata.utils.domain.MetadataTools;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.commons.metadata.utils.xml.RBVNamespaces;

@XmlRootElement(name = "MD_Metadata", namespace = RBVNamespaces.GMD_NAMESPACE)
public class RBVMetadata extends SedooMetadata
{
	public final static String OBSERVATORY_RBV_NAMESPACE = "http://portailrbvws.sedoo.fr/observatory/";
	public final static String EXPERIMENTAL_SITE_RBV_NAMESPACE = "http://portailrbvws.sedoo.fr/experimentalsite/";
	public final static String LOGO = "logo";
	public static final String OBSERVATORY_LEVEL = "observatoryLevel";
	public static final String EXPERIMENTAL_SITE_LEVEL = "experimentalSiteLevel";
	public static final String DATASET_LEVEL = "datasetLevel";

	public RBVMetadata() {
		setDatasetLevel();
	}
	
	
	public void setDatasetLevel()
	{
		Collection<ScopeCode> hierarchyLevels = new ArrayList<ScopeCode>();
		hierarchyLevels.add(ScopeCode.DATASET);
		setHierarchyLevels(hierarchyLevels);
		
		Collection<String> hierarchyLevelNames = new ArrayList<String>();
		setHierarchyLevelNames(hierarchyLevelNames);
	}
	
	public void setObservatoryLevel()
	{
		Collection<ScopeCode> hierarchyLevels = new ArrayList<ScopeCode>();
		hierarchyLevels.add(ScopeCode.SERIES);
		setHierarchyLevels(hierarchyLevels);
		
		Collection<String> hierarchyLevelNames = new ArrayList<String>();
		hierarchyLevelNames.add(OBSERVATORY);
		setHierarchyLevelNames(hierarchyLevelNames);
	}
	
	public void setExperimentalSiteLevel()
	{
		Collection<ScopeCode> hierarchyLevels = new ArrayList<ScopeCode>();
		hierarchyLevels.add(ScopeCode.SERIES);
		setHierarchyLevels(hierarchyLevels);
		
		Collection<String> hierarchyLevelNames = new ArrayList<String>();
		hierarchyLevelNames.add(EXPERIMENTAL_SITE);
		setHierarchyLevelNames(hierarchyLevelNames);
	}
	
	public void setLevel(Level level)
	{
		if (level.getType().compareToIgnoreCase(OBSERVATORY)==0)
		{
			setObservatoryLevel();
			setObservatoryName(OBSERVATORY_LEVEL);
		}

		else if (level.getType().compareToIgnoreCase(EXPERIMENTAL_SITE)==0)
		{
			setExperimentalSiteLevel();
			setExperimentalSiteName(EXPERIMENTAL_SITE_LEVEL);
		}
	}
	
	public void setLogo(Logo logo)
	{
		DescribedURL aux = new DescribedURL();
		aux.setLabel(LOGO);
		aux.setLink(StringUtils.trimToEmpty(logo.getUrl()));
		addSnapshotURL(Collections.singletonList(aux));
	}
	
	public void setObservatoryName(String string) 
	{
		DefaultResourceIdentifier identifier = new DefaultResourceIdentifier();
		identifier.setCode(string.toUpperCase());
		identifier.setNameSpace(OBSERVATORY_RBV_NAMESPACE);
		addResourceIdentifiers(Collections.singletonList(identifier));
	}
	
	public void setExperimentalSiteName(String string) 
	{
		DefaultResourceIdentifier identifier = new DefaultResourceIdentifier();
		identifier.setCode(string.toUpperCase());
		identifier.setNameSpace(EXPERIMENTAL_SITE_RBV_NAMESPACE);
		addResourceIdentifiers(Collections.singletonList(identifier));
	}

	public void setDefaultLanguage(String language)
	{
		setMetadataLanguage(language);
	}

	public void setSimpleResourceTitle(String value)
	{
		if (StringUtils.isEmpty(value)==false)
		{
			DefaultCitation citation = MetadataTools.getCitation(this);
			citation.setTitle(new SimpleInternationalString(StringUtils.trimToEmpty(value)));
		}
	}
	
	public void setSimpleResourceAbstract(String value)
	{
		if (StringUtils.isEmpty(value)==false)
		{
			DefaultDataIdentification dataIdentification = MetadataTools.getFisrtIdentificationInfo(this);
			dataIdentification.setAbstract(new SimpleInternationalString(StringUtils.trimToEmpty(value)));
		}
	}
	
	public void setSimpleUseConditions(String value)
	{
		if (StringUtils.isEmpty(value)==false)
		{
			DefaultConstraints useConditionConstraint = MetadataTools.getUseConditionConstraint(this);
			useConditionConstraint.setUseLimitations(Collections.singletonList(new SimpleInternationalString(StringUtils.trimToEmpty(value))));
		}
	}
	
	public void setSimplePublicAccessLimitations(String value)
	{
		if (StringUtils.isEmpty(value)==false)
		{
			DefaultLegalConstraints legalConstraints = MetadataTools.getPublicAccessLimitationConstraint(this);
			legalConstraints.setOtherConstraints(Collections.singletonList(new SimpleInternationalString(StringUtils.trimToEmpty(value))));
		}
	}
	
	public void setI18nResourceTitle(RBVI18nString value)
	{
		HashMap<String, String> translations  = value.getTranslationAsHashMap();
		String defautValue = StringUtils.trimToEmpty(translations.get(getMetadataLanguage()));
		setResourceTitle(MetadataTools.toLocaleMap(translations), defautValue);
	}
	
	public void setI18nResourceAbstract(RBVI18nString value)
	{
		HashMap<String, String> translations  = value.getTranslationAsHashMap();
		String defautValue = StringUtils.trimToEmpty(translations.get(getMetadataLanguage()));
		setResourceAbstract(MetadataTools.toLocaleMap(translations), defautValue);
	}
	
	public void setI18nUseConditions(RBVI18nString value)
	{
		HashMap<String, String> translations  = value.getTranslationAsHashMap();
		String defautValue = StringUtils.trimToEmpty(translations.get(getMetadataLanguage()));
		setUseConditions(MetadataTools.toLocaleMap(translations), defautValue);
	}
	
	public void setI18nPublicAccessLimitations(RBVI18nString value)
	{
		HashMap<String, String> translations  = value.getTranslationAsHashMap();
		String defautValue = StringUtils.trimToEmpty(translations.get(getMetadataLanguage()));
		setPublicAccessLimitations(MetadataTools.toLocaleMap(translations), defautValue);
	}
	
	
	public static String getObservatoryNameFromSummary(Summary summary)
	{
		List<ResourceIdentifier> identifiers = summary.getIdentifiers();
		if (identifiers != null)
		{
			Iterator<ResourceIdentifier> iterator = identifiers.iterator();
			while (iterator.hasNext()) {
				ResourceIdentifier resourceIdentifier = iterator.next();
				if (resourceIdentifier.getNameSpace().compareToIgnoreCase(OBSERVATORY_RBV_NAMESPACE)==0)
				{
					return StringUtils.trimToEmpty(resourceIdentifier.getCode());
				}
			}
		}
		return "";
	}
	
	public static String getExperimentalSiteNameFromSummary(Summary summary)
	{
		List<ResourceIdentifier> identifiers = summary.getIdentifiers();
		if (identifiers != null)
		{
			Iterator<ResourceIdentifier> iterator = identifiers.iterator();
			while (iterator.hasNext()) {
				ResourceIdentifier resourceIdentifier = iterator.next();
				if (resourceIdentifier.getNameSpace().compareToIgnoreCase(EXPERIMENTAL_SITE_RBV_NAMESPACE)==0)
				{
					return StringUtils.trimToEmpty(resourceIdentifier.getCode());
				}
			}
		}
		return "";
	}
	
	
}

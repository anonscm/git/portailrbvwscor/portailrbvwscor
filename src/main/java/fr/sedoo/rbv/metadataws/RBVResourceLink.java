package fr.sedoo.rbv.metadataws;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.metadata.shared.DefaultResourceLink;
import fr.sedoo.commons.metadata.shared.ResourceLink;


public class RBVResourceLink extends DefaultResourceLink 
{
	private static final String WMS = "wms";
	private static final String WFS = "wfs";
	private static final String DOWNLOAD = "download";
	private static final String INFORMATION = "information";

	@Override
	public void setProtocol(String protocol) 
	{
		String aux = StringUtils.trimToEmpty(protocol).toLowerCase();
		if (aux.compareTo(INFORMATION)==0)
		{
			super.setProtocol(ResourceLink.LINK_PROTOCOL);
		}
		else if (aux.compareTo(DOWNLOAD)==0)
		{
			super.setProtocol(ResourceLink.DOWNLOAD_PROTOCOL);
		}
		else if (aux.compareTo(WFS)==0)
		{
			super.setProtocol(ResourceLink.WFS_SERVICE);
		}
		else if (aux.compareTo(WMS)==0)
		{
			super.setProtocol(ResourceLink.WMS_SERVICE);
		}
		else if (StringUtils.isEmpty(aux))
		{
			super.setProtocol(ResourceLink.LINK_PROTOCOL);
		}
		else
		{
			super.setProtocol(aux);
		}
	}

public static String getRBVProtocol(String protocol)
{
	String aux = StringUtils.trimToEmpty(protocol).toLowerCase();
	if (aux.compareTo(ResourceLink.LINK_PROTOCOL.toLowerCase())==0)
	{
		return INFORMATION;
	}
	else if (aux.compareTo(ResourceLink.DOWNLOAD_PROTOCOL.toLowerCase())==0)
	{
		return DOWNLOAD;
	}
	else if (aux.compareTo(ResourceLink.WFS_SERVICE.toLowerCase())==0)
	{
		return WFS;
	}
	else if (aux.compareTo(ResourceLink.WMS_SERVICE.toLowerCase())==0)
	{
		return WMS;
	}
	else if (StringUtils.isEmpty(aux))
	{
		return INFORMATION;
	}
	else
	{
		return aux;
	}
}
}

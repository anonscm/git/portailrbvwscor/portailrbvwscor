package fr.sedoo.rbv.metadataws;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;
import org.xml.sax.InputSource;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;

public class RBVUnmarshaller
{
	public static SedooMetadata fromRBV(String xml) throws Exception
	{
		
		DigesterLoader digesterLoader = DigesterLoader.newLoader(new RBVModule());
		Digester digester = digesterLoader.newDigester();
        digester.setValidating(false);
        
       	SedooMetadata result = digester.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));
       	//new ByteArrayInputStream(xml.getBytes())
       	
       	return result;
	}
	
}

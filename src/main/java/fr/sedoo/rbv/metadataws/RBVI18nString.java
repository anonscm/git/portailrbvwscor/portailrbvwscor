package fr.sedoo.rbv.metadataws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class RBVI18nString {
	
	public RBVI18nString() {
	}
	
	private ArrayList<Translation> translations = new ArrayList<Translation>();

	public ArrayList<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(ArrayList<Translation> translations) {
		this.translations = translations;
	}
	
	public void addTranslation(Translation translation)
	{
		translations.add(translation);
	}

	public HashMap<String, String> getTranslationAsHashMap() {
		HashMap<String, String> result = new HashMap<String, String>();
		Iterator<Translation> iterator = translations.iterator();
		while (iterator.hasNext()) 
		{
			Translation translation = (Translation) iterator.next();
			result.put(translation.getLanguage(), translation.getContent());
		}
		return result;
	}


}

package fr.sedoo.rbv.metadataws;

import java.util.ArrayList;

import org.apache.commons.digester3.binder.AbstractRulesModule;
import org.geotoolkit.metadata.iso.extent.DefaultGeographicBoundingBox;

import fr.sedoo.commons.metadata.shared.DefaultResourceIdentifier;
import fr.sedoo.commons.metadata.utils.domain.Contact;
import fr.sedoo.commons.metadata.utils.domain.DescribedURL;
import fr.sedoo.commons.metadata.utils.domain.WrappedString;

public class RBVModule extends AbstractRulesModule implements RBVConstants {

	@Override
	public void configure() {

		forPattern(METADATA_NODE_NAME).createObject().ofType(RBVMetadata.class).then().setProperties();
		
		forPattern(METADATA_NODE_NAME+"/"+LEVEL_NODE_NAME).createObject().ofType(Level.class).then().setProperties().then().setNext("setLevel");
		forPattern(METADATA_NODE_NAME+"/"+LOGO_NODE_NAME).createObject().ofType(Logo.class).then().setProperties().then().setNext("setLogo");
		
		/* Identification */
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_TITLE_NODE_NAME).setBeanProperty().withName("simpleResourceTitle");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_I18N_TITLE_NODE_NAME).createObject().ofType(RBVI18nString.class).then().setProperties().then().setNext("setI18nResourceTitle");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_I18N_TITLE_NODE_NAME+"/"+TRANSLATION_NODE_NAME).createObject().ofType(Translation.class).then().setProperties().then().setBeanProperty().withName("content").then().setNext("addTranslation");

		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_ABSTRACT_NODE_NAME).setBeanProperty().withName("simpleResourceAbstract");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_I18N_ABSTRACT_NODE_NAME).createObject().ofType(RBVI18nString.class).then().setProperties().then().setNext("setI18nResourceAbstract");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_I18N_ABSTRACT_NODE_NAME+"/"+TRANSLATION_NODE_NAME).createObject().ofType(Translation.class).then().setProperties().then().setBeanProperty().withName("content").then().setNext("addTranslation");
		
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_STATUS_NODE_NAME).setBeanProperty().withName("status");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_UPDATE_RYTHM_NODE_NAME).setBeanProperty().withName("resourceUpdateRythm");
		
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_SNAPSHOTS_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("addSnapshotURL");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_SNAPSHOTS_NODE_NAME+"/"+RESOURCE_SNAPSHOT_NODE_NAME).createObject().ofType(DescribedURL.class).then().setProperties().then().setNext("add");
		
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_URLS_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("setResourceURL");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+RESOURCE_URLS_NODE_NAME+"/"+RESOURCE_URL_NODE_NAME).createObject().ofType(RBVResourceLink.class).then().setProperties().then().setNext("add");
		
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+IDENTIFIERS_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("addResourceIdentifiers");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+IDENTIFIERS_NODE_NAME+"/"+IDENTIFIER_NODE_NAME).createObject().ofType(DefaultResourceIdentifier.class).then().setProperties().then().setNext("add");
		
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+CONTACTS_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("setResourceContacts");
		forPattern("*/"+IDENTIFICATION_NODE_NAME+"/"+CONTACTS_NODE_NAME+"/"+CONTACT_NODE_NAME).createObject().ofType(Contact.class).then().setProperties().addAlias(CONTACT_EMAIL_ATTR_NAME).forProperty("emailAddress").addAlias(CONTACT_ORGANISATION_ATTR_NAME).forProperty("organisationName").addAlias(CONTACT_INDIVIDUAL_ATTR_NAME).forProperty("individualName").addAlias(CONTACT_ROLE_ATTR_NAME).forProperty("role").then().setNext("add");
		
		/* Metametadata */
		forPattern("*/"+METAMETADATA_NODE_NAME+"/"+METADATA_UUID_NODE_NAME).setBeanProperty().withName("uuid");
		forPattern("*/"+METAMETADATA_NODE_NAME+"/"+METADATA_PARENT_UUID_NODE_NAME).setBeanProperty().withName("parentIdentifier");
		forPattern("*/"+METAMETADATA_NODE_NAME+"/"+METADATA_LAST_MODIFICATION_DATE_NODE_NAME).setBeanProperty().withName("metadataDate");
		forPattern("*/"+METAMETADATA_NODE_NAME+"/"+CONTACTS_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("setMetadataContacts");
		forPattern("*/"+METAMETADATA_NODE_NAME+"/"+CONTACTS_NODE_NAME+"/"+CONTACT_NODE_NAME).createObject().ofType(Contact.class).then().setProperties().addAlias(CONTACT_EMAIL_ATTR_NAME).forProperty("emailAddress").addAlias(CONTACT_ORGANISATION_ATTR_NAME).forProperty("organisationName").addAlias(CONTACT_INDIVIDUAL_ATTR_NAME).forProperty("individualName").addAlias(CONTACT_ROLE_ATTR_NAME).forProperty("role").then().setNext("add");

		/* Spatial */
		forPattern("*/"+LOCALISATION_NODE_NAME+"/"+GEOGRAPHICAL_BOX_NODE_NAME).createObject().ofType(DefaultGeographicBoundingBox.class).then().setProperties().addAlias(WEST_ATTR_NAME).forProperty("westBoundLongitude").addAlias(EAST_ATTR_NAME).forProperty("eastBoundLongitude").addAlias(NORTH_ATTR_NAME).forProperty("northBoundLatitude").addAlias(SOUTH_ATTR_NAME).forProperty("southBoundLatitude").then().setNext("addGeographicBoundingBox");
				
		/* Temporal */
		forPattern("*/"+TEMPORAL_NODE_NAME+"/"+RESOURCE_BEGIN_DATE).setBeanProperty().withName("resourceBeginDate");
		forPattern("*/"+TEMPORAL_NODE_NAME+"/"+RESOURCE_END_DATE).setBeanProperty().withName("resourceEndDate");
		
		/* Constraint */
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+USE_CONDITIONS_NODE_NAME).setBeanProperty().withName("simpleUseConditions");
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+I18N_USE_CONDITIONS_NODE_NAME).createObject().ofType(RBVI18nString.class).then().setProperties().then().setNext("setI18nUseConditions");
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+I18N_USE_CONDITIONS_NODE_NAME+"/"+TRANSLATION_NODE_NAME).createObject().ofType(Translation.class).then().setProperties().then().setBeanProperty().withName("content").then().setNext("addTranslation");
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+PUBLIC_ACCESS_LIMITATIONS_NODE_NAME).setBeanProperty().withName("simplePublicAccessLimitations");
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+I18N_PUBLIC_ACCESS_LIMITATIONS_NODE_NAME).createObject().ofType(RBVI18nString.class).then().setProperties().then().setNext("setI18nPublicAccessLimitations");
		forPattern("*/"+CONSTRAINTS_NODE_NAME+"/"+I18N_PUBLIC_ACCESS_LIMITATIONS_NODE_NAME+"/"+TRANSLATION_NODE_NAME).createObject().ofType(Translation.class).then().setProperties().then().setBeanProperty().withName("content").then().setNext("addTranslation");
		
		/* Others */
		forPattern("*/"+OTHERS_NODE_NAME+"/"+LANGUAGES_NODE_NAME).createObject().ofType(ArrayList.class).then().setProperties().then().setNext("setResourceLanguagesFromWrappedString");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+LANGUAGES_NODE_NAME+"/"+LANGUAGE_NODE_NAME).createObject().ofType(WrappedString.class).then().setBeanProperty().withName("value").then().setNext("add");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+RESOURCE_ENCODING_CHARSET_NODE_NAME).setBeanProperty().withName("resourceEncodingCharset");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+RESOURCE_FORMAT_NODE_NAME).createObject().ofType(RBVFormat.class).then().setProperties().addAlias(NAME_ATTR_NAME).forProperty("stringName").addAlias(VERSION_ATTR_NAME).forProperty("stringVersion").then().setNext("setResourceFormat");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+CREATION_DATE_NODE_NAME).setBeanProperty().withName("creationDate");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+LAST_REVISION_DATE_NODE_NAME).setBeanProperty().withName("lastRevisionDate");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+PUBLICATION_DATE_NODE_NAME).setBeanProperty().withName("publicationDate");
		forPattern("*/"+OTHERS_NODE_NAME+"/"+COORDINATE_SYSTEM_NODE_NAME).setBeanProperty().withName("coordinateSystem");
	}
	
	

}
